import User from '../models/User'
import * as Yup from 'yup'
import jwt from 'jsonwebtoken'

class SessionController {
  async store(req, res) {
    const schema = Yup.object().shape({
      email: Yup.string().email().required(),
      password: Yup.string().required(),
    })

    const userEmailOrPasswordIncorrect = () => {
      return res
        .status(401)
        .json({ error: 'Make sure your password or email are correct' })
    }

    if (!(await schema.isValid(req.body))) return userEmailOrPasswordIncorrect()

    const { email, password } = req.body

    const user = await User.findOne({
      where: { email },
    })

    if (!user) return userEmailOrPasswordIncorrect()

    if (!(await user.checkPassword(password)))
      return userEmailOrPasswordIncorrect()

    return res.json({
      id: user.id,
      name: user.name,
      email: user.email,
      admin: user.admin,
      token: jwt.sign({ id: user.id }, process.env.TOKEN_SECRET, {
        expiresIn: process.env.TOKEN_EXPIRATION,
      }),
    })
  }

  async update(req, res) {
    const schema = Yup.object().shape({
      oldPassword: Yup.string().required(),
      password: Yup.string().required().min(6),
      confirmPassword: Yup.string().required().min(6),
    })

    if (!(await schema.isValid(req.body)))
      return res.status(400).json({ error: 'Validation fails' })

    const { oldPassword, password, confirmPassword } = req.body

    if (password !== confirmPassword)
      return res.status(400).json({ error: 'Password does not match' })

    const user = await User.findByPk(req.userId)

    if (!(await user.checkPassword(oldPassword)))
      return res.status(401).json({ error: 'Password does not match' })

    await user.update({ password })

    return res.json({ message: 'Password updated successfully' })
  }
}

export default new SessionController()
