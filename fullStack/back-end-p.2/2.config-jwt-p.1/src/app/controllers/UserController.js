/*
store => create
index => list
show => list one
update => update
destroy => delete
*/

import User from '../models/User'
import { v4 } from 'uuid'
import * as Yup from 'yup'

class UserController {
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      email: Yup.string().email().required(),
      password: Yup.string().required().min(8),
      admin: Yup.boolean(),
    })

    /** validação básica
     * if (!(await schema.isValid(req.body))) {
     *  return res.status(400).json({ error: 'Validation fails' })
     * }
     */

    try {
      await schema.validateSync(req.body, { abortEarly: false })
    } catch (err) {
      console.log(err)
      return res.status(400).json({ error: err.errors })
    }

    const { name, email, password, admin } = req.body

    const userExists = await User.findOne({ where: { email } })

    if (userExists) {
      return res.status(400).json({ error: 'User already exists' })
    }

    const user = await User.create({
      id: v4(),
      name,
      email,
      password,
      admin,
    })

    return res.json({ id: user.id, name, email, admin }).status(201)
  }

  async index(req, res) {
    const users = await User.findAll()

    const usersList = users.map((user) => {
      return {
        id: user.id,
        name: user.name,
        email: user.email,
        admin: user.admin,
      }
    })

    return res.json(usersList)
  }
}

export default new UserController()
