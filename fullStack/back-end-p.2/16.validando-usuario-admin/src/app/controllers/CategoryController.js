import * as yup from 'yup'
import Category from '../models/Category'
import User from '../models/User'

class CategoryController {
  async store (req, res) {
    const schema = yup.object().shape({
      name: yup.string().required()
    })

    try {
      schema.validateSync(req.body, { abortEarly: false })
    } catch (err) {
      return res.status(400).json({ error: err.errors })
    }

    const { admin: isAdmin } = await User.findByPk(req.userId)

    if (!isAdmin) {
      return res.status(401).json({ error: 'User is not an admin' })
    }

    console.log(isAdmin)

    const { name } = req.body

    const categoryExists = await Category.findOne({ where: { name } })

    if (categoryExists) {
      return res.status(400).json({ error: 'Category already exists' })
    }

    const category = await Category.create({ name })

    return res.json({ id: category.id, name: category.name })
  }

  async index (req, res) {
    const category = await Category.findAll()

    return res.json(category)
  }
}

export default new CategoryController()
