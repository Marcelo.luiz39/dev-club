import * as yup from 'yup'
import Category from '../models/Category'

class CategoryController {
  async store(req, res) {
    const schema = yup.object().shape({
      name: yup.string().required(),
    })

    try {
      schema.validateSync(req.body, { abortEarly: false })
    } catch (err) {
      return res.status(400).json({ error: err.errors })
    }

    const { name } = req.body

    const categoryExists = await Category.findOne({ where: { name } })

    if (categoryExists) {
      return res.status(400).json({ error: 'Category already exists' })
    }

    const category = await Category.create({ name })

    return res.json(category)
  }

  async index(req, res) {
    const category = await category.findAll()

    return res.json(category)
  }
}

export default new CategoryController()
