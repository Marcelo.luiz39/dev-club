/*
store => create
index => list
show => list one
update => update
destroy => delete
*/

import User from '../models/User'
import { v4 } from 'uuid'
import * as Yup from 'yup' // pegar tudo do Yup e jogar dentro da variável Yup

class UserController {
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      email: Yup.string().email().required(),
      password_hash: Yup.string().required().min(6),
      admin: Yup.boolean(),
    })

    //  validação básica
    // if (!(await schema.isValid(req.body))) {
    //   return res.status(400).json({ error: 'Validation fails' })
    // }

    try {
      schema.validateSync(req.body, { abortEarly: false })
    } catch (err) {
      return res.status(400).json({ error: err.errors })
    }

    const { name, email, password_hash, admin } = req.body

    const user = await User.create({
      id: v4(),
      name,
      email,
      password_hash,
      admin,
    })

    return res.json({ id: user.id, name, email, admin }).status(201)
  }
}

export default new UserController()
