import Sequelize from 'sequelize'

import configDatabase from '../config/database'
import User from '../app/models/User'

//usando o array de models para fazer a importação de todos os models
const models = [User]

class Database {
  constructor() {
    this.init()
  }

  init() {
    this.connection = new Sequelize(configDatabase)
    //percorrendo o array de models e fazendo a importação de cada um
    models.map((model) => model.init(this.connection))
  }
}

export default new Database()
