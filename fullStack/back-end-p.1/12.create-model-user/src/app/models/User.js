import Sequelize, { Model } from 'sequelize'

class User extends Model {
  // Método estático chamado pelo Sequelize automaticamente
  static init(sequelize) {
    // Chamando o método init da classe pai (Model) do sequelize com o static não preciso instanciar a classe
    super.init(
      {
        name: Sequelize.STRING,
        email: Sequelize.STRING,
        password_hash: Sequelize.STRING,
        admin: Sequelize.BOOLEAN,
      },
      {
        // Passando a conexão com o banco de dados
        sequelize,
      }
    )
  }
}
export default User
