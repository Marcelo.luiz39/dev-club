import User from '../models/User'
import * as Yup from 'yup'

class SessionController {
  async store(req, res) {
    const schema = Yup.object().shape({
      email: Yup.string().email().required(),
      password: Yup.string().required(),
    })

    const incorrectEmailOrPassword = () => {
      return res
        .status(401)
        .json({ error: 'Make sure your password or email are correct' })
    }

    if (!(await schema.isValid(req.body))) return incorrectEmailOrPassword()

    const { email, password } = req.body

    const user = await User.findOne({ where: { email } })

    if (!user) return incorrectEmailOrPassword()

    if (!(await user.checkPassword(password))) return incorrectEmailOrPassword()

    return res
      .json({
        user: {
          id: user.id,
          name: user.name,
          email: user.email,
          password: user.password_hash,
        },
      })
      .status(200)
  }
}

export default new SessionController()
