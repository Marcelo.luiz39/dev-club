/*
store => create
index => list
show => list one
update => update
destroy => delete
*/

import User from '../models/User'
import { v4 } from 'uuid'

class UserController {
  async store(req, res) {
    const userExists = await User.findOne({
      where: { email: req.body.email },
    })

    if (userExists) {
      return res.status(400).json({
        error: 'User already exists.',
      })
    }

    const { name, email, password_hash, admin } = req.body

    const user = await User.create({
      id: v4(),
      name,
      email,
      password_hash,
      admin,
    })

    return res.status(201).json({
      id: user.id,
      name,
      email,
      admin,
    })
  }

  async index(req, res) {
    const users = await User.findAll()

    return res.status(200).json(users)
  }
}

export default new UserController()
