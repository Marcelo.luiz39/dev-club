import React from 'react'

import Register from './containers/Register'
import Login from './containers/Login'
import GlobalStyles from './styles/globalStyles'
import { ToastContainer, toast } from 'react-toastify'

function App() {
	return (
		<div>
			<Register />
			<ToastContainer autoClose={2500} theme={'colored'} />
			{/* <Login /> */}
			<GlobalStyles />
		</div>
	)
}

export default App
