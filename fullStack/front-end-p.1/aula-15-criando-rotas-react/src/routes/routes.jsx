import {Switch, Route, BrowserRouter as Router} from 'react-router-dom'

import Login from '../pages/Login'
import Register from '../pages/Register'

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/login" component={Login} />
        <Route exact path="/cadastro" component={Register} />
      </Switch>
    </Router>
  )
}

export default Routes