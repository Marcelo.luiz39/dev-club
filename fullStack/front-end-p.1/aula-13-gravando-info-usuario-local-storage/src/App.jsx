import React from 'react'

import Register from './pages/Register'
import Login from './pages/Login'
import GlobalStyles from './styles/globalStyles'
import { ToastContainer } from 'react-toastify'

function App() {
	return (
		<div>
			{/* <Register /> */}
			<ToastContainer autoClose={2500} theme={'colored'} />
			<Login />
			<GlobalStyles />
		</div>
	)
}

export default App
