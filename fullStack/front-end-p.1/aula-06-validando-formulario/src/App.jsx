import React from 'react'

import Login from './containers/Login'
import GlobalStyles from './styles/globalStyles'

function App() {
  return (
    <div>
      <Login />
      <GlobalStyles />
    </div>
  )
}

export default App
