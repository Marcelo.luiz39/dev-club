/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable import/no-extraneous-dependencies */
import React from 'react'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as Yup from 'yup'
import {
  Container,
  ContainerImage,
  ContainerItens,
  Label,
  Input,
  ErrorMessage,
  Button,
  SignInLink,
} from './styles'

import Logo from '../../assets/logo.svg'

import api from '../../services/api'

function login() {
  const schema = Yup.object().shape({
    email: Yup.string()
      .email('Digite um email valido!')
      .required('O email é obrigatório!'),
    password: Yup.string()
      .required('A senha é obrigatória!')
      .min(8, 'No mínimo 8 caracteres!'),
  })

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  })

  const onSubmit =  async(dataClient) => {
    try {
      const response = await api.post('sessions', dataClient)
      console.log(response.data)
    } catch (error) {
      console.log(error)
    }
  }
    

  return (
    <Container>
      <ContainerImage />
      <ContainerItens>
        <img src={Logo} alt="logo" />
        <h1>Login</h1>

        <form noValidate onSubmit={handleSubmit(onSubmit)}>
          <Label>
            Email
            <Input
              type="email"
              {...register('email')}
              error={errors.email?.message}
              placeholder="Digite seu email"
            />
            <ErrorMessage>{errors.email?.message}</ErrorMessage>
          </Label>

          <Label>
            Senha
            <Input
              type="password"
              {...register('password')}
              error={errors.password?.message}
              placeholder="Digite sua senha"
            />
            <ErrorMessage>{errors.password?.message}</ErrorMessage>
          </Label>

          <Button type="submit">Entrar</Button>

          <SignInLink>
            Não tem uma conta ? <a href='#'>Cadastre-se</a>
          </SignInLink>
        </form>
      </ContainerItens>
    </Container>
  )
}

export default login
