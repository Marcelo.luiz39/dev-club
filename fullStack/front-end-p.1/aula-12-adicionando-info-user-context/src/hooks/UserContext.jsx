import React, { createContext, useContext, useState } from 'react'

//colocar o nome do contexto em maiúsculo
const UserContext = createContext({})

//criar um hook para usar o contexto
export const UserProvider = ({ children }) => {
	const [userData, setUserData] = useState({})

  const putUserData = (userValue) => {
    setUserData(userValue)
  }

	return (
		<UserContext.Provider value={{ putUserData, userData }}>
			{children}
		</UserContext.Provider>
	)
}

//exportar o contexto e o provider e criando um hook para usar o contexto
export const useUser = () => {
	const context = useContext(UserContext)
	if (!context) {
		throw new Error('useUser must be used within a UserProvider')
	}
	return context
}
