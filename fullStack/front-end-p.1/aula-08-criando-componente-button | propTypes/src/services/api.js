import axios from 'axios'

const apiDevBurger = axios.create({
  baseURL: 'https://apidevburger.fly.dev',
})

export default apiDevBurger
