import React, { useEffect, useState } from 'react'

import Offers from '../../assets/offers.svg'
import api from '../../services/api'

import { useMediaQuery } from 'react-responsive'
import { Container, OfferImg, Image, ContainerItems, Button } from './styles'
import Carousel from 'react-elastic-carousel'

const OffersCarousel = () => {
	const [offers, setOffers] = useState([])

	const isMobile = useMediaQuery({ query: '(max-width: 768px)' })
	const itemsToShow = isMobile ? 2 : 4

	const breakPoints = [
		{ width: 1, itemsToShow: 1 },
		{ width: 768, itemsToShow: 3 },
		{ width: 1200, itemsToShow: 4 }
	]

	useEffect(() => {
		;(async () => {
			const { data } = await api.get('products')

			const filteredData = data.filter((product) => product.offer === true)

			setOffers(filteredData)
			console.log(data)
		})()
	}, [])

	return (
		<Container>
			<OfferImg src={Offers} alt="logo das ofertas" />
			<Carousel itemsToShow={itemsToShow} breakPoints={breakPoints}>
				{offers &&
					offers.map((product) => (
						<ContainerItems key={product.id}>
							<Image src={product.url} alt="foto das ofertas" />

							<p>{product.name}</p>
							<span>R$ {product.price}</span>

							<Button>
								<span>Peça agora</span>
							</Button>
						</ContainerItems>
					))}
			</Carousel>
		</Container>
	)
}

export default OffersCarousel
