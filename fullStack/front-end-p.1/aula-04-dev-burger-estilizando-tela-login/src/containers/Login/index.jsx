import React from 'react'
import {
  Container,
  ContainerImage,
  ContainerItens,
  Label,
  Input,
  Button,
  SignInLink,
} from './styles'

import Logo from '../../assets/logo.svg'

function login() {
  return (
    <Container>
      <ContainerImage />
      <ContainerItens>
        <img src={Logo} alt="logo"/>
        <h1>Login</h1>

        <Label>Email
        <Input />
        </Label>

        <Label>Senha
        <Input />
        </Label>

        <Button>Entrar</Button>
        <SignInLink>
          Não tem uma conta ? <a>Cadastre-se</a>
        </SignInLink>
      </ContainerItens>
    </Container>
  )
}

export default login
