import styled from 'styled-components'
import Hamburger from '../../assets/hamburger.svg'

export const Container = styled.div`
  display: flex;
  height: 100vh;
  width: 100vw;
  background: #373737;
`
export const ContainerImage = styled.div`
 flex: 1;
  background: url(${Hamburger}) no-repeat;
`
export const ContainerItens = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  h1 {
    margin-top: 100px;
    margin-bottom: 17px;
    color: #fff;
    font-style: normal;
    font-weight: 500;
    font-size: 24px;
    line-height: 28px;
  }
`
export const Label = styled.label`
  display: flex;
  flex-direction: column;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 14px;

  margin-bottom: 25px;

  color: #ffffff;
`
export const Input = styled.input`
  width: 391.42px;
  height: 38.32px;
  margin: 5px 0;
  background: #ffffff;
  border-radius: 5px;
  border: none;
`
export const Button = styled.button`
  width: 182.81px;
  height: 36.13px;

  color: #fff;

  background: #9758a6;
  border-radius: 20px;
  border: none;
  margin-top: 66px;
  margin-bottom: 32px;

  &:hover {
    opacity: 0.8;
  }

  &:active {
    opacity: 0.6;
  }
`
export const SignInLink = styled.p`
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  color: #ffffff;

  a{
    cursor: pointer;
    text-decoration:underline;
    font-weight: 300;
  }
`
