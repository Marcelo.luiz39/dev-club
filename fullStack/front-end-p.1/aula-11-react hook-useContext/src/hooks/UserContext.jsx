import React, { createContext, useContext } from 'react'

//colocar o nome do contexto em maiúsculo
const UserContext = createContext({})

export const UserProvider = ({ children }) => {
	const user = { name: 'Marcelo Luiz', age: 43 }
	const newUser = { name: 'Julia Souza', age: 19 }

	return (
		<UserContext.Provider value={{ user, newUser }}>
			{children}
		</UserContext.Provider>
	)
}

export const useUser = () => {
	const context = useContext(UserContext)
	if (!context) {
		throw new Error('useUser must be used within a UserProvider')
	}
	return context
}

/*
criar um hook para usar o contexto
const UserProvider = ({ children }) => {
  const [user, setUser] = React.useState(null)

  return (
    <UserContext.Provider value={{ user, setUser }}>
      {children}
    </UserContext.Provider>
  )
}
*/
