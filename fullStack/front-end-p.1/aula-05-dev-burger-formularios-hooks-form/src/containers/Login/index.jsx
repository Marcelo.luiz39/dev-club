import React from 'react'
import { useForm } from 'react-hook-form'
import {
  Container,
  ContainerImage,
  ContainerItens,
  Label,
  Input,
  Button,
  SignInLink,
} from './styles'

import Logo from '../../assets/logo.svg'

function login() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()
  const onSubmit = (data) => console.log(data)
  return (
    <Container>
      <ContainerImage />
      <ContainerItens>
        <img src={Logo} alt="logo" />
        <h1>Login</h1>

        <form onSubmit={handleSubmit(onSubmit)}>
          <Label>
            Email
            <Input type="email" {...register('email')} />
          </Label>

          <Label>
            Senha
            <Input type='password' {...register('password')} />
          </Label>

          <Button type="submit">Entrar</Button>
        
        <SignInLink>
          Não tem uma conta ? <a>Cadastre-se</a>
        </SignInLink>
        </form>
      </ContainerItens>
    </Container>
  )
}

export default login
