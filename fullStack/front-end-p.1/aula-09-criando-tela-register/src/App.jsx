import React from 'react'

import Register from './containers/Register'
import Login from './containers/Login'
import GlobalStyles from './styles/globalStyles'

function App() {
  return (
    <div>
      <Register />
      {/* <Login /> */}
      <GlobalStyles />
    </div>
  )
}

export default App
