import React, { useEffect } from 'react'

import Category from '../../assets/category-home.png'

import { Container, HomeImg } from './styles'
import api from '../../services/api'

const CategoryCarousel = () => {
	useEffect(() => {
		;(async () => {
			const response = await api
				.get('/categories')
				.then((response) => console.log(response))
		})()
	}, [])

	return (
		<Container>
			<HomeImg src={Category} alt="imagem da categorias" />
		</Container>
	)
}

export default CategoryCarousel
