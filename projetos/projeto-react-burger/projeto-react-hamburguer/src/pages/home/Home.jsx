import React from 'react'
import './style.css'
import Burger from '@/assets/burger.svg'

export default function Home() {
  return (
    <div className="container container-order">
      <img
        src={Burger}
        alt="imagem de hambúrguer"
        className="img-fluid mx-auto d-block mb-3"
      />
      <h1 className="title">Faça seu pedido!</h1>
      <div className="container">
        <section className="container d-flex flex-column align-items-center">
          <section className="value-client d-flex flex-column align-items-start">
            <label htmlFor="name" className="label-order">
              Nome do Cliente
            </label>
            <input
              type="text"
              className="input-name"
              placeholder="Digite seu nome"
            />
          </section>
          <section className="value-order d-flex flex-column align-items-start">
            <label htmlFor="email" className="label-order">
              Pedido
            </label>
            <div>
              <select className="select-hamburger">
                <option defaultValue="DEFAULT">Escolha seu Hamburger</option>
                <option value="1">X-Burger</option>
                <option value="2">X-Salada</option>
                <option value="3">X-Bacon</option>
              </select>
            </div>
            <div>
              <select className="select-drink">
                <option defaultValue="DEFAULT">Escolha sua Bebida</option>
                <option value="1">Coca-cola</option>
                <option value="2">Fanta</option>
                <option value="3">Guaraná</option>
                <option value="3">Suco</option>
              </select>
            </div>
            <div>
              <select className="select-follow-up">
                <option defaultValue="DEFAULT">
                  Escolha seu Acompanhamento
                </option>
                <option value="1">Batata Frita</option>
                <option value="2">Milk-shake</option>
                <option value="3">Chicken Nuggets</option>
              </select>
            </div>
          </section>
        </section>
      </div>
      <div className="container-button">
        <button className="button-order">Fazer Pedido</button>
      </div>
    </div>
  )
}
