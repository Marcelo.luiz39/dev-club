import styled from 'styled-components'

export const Button = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 14px;

  width: 342px;
  height: 74px;

  background: ${props => props.isBack ? 'transparent' : 'rgba(0, 0, 0, 0.8)'};
  border: ${props => (props.isBack ? '1px solid #fff' : 'none')};
  border-radius: 14px;

  font-style: normal;
  font-weight: 700;
  font-size: 17px;
  line-height: 28px;

  text-decoration: none;

  cursor: pointer;

  color: #ffffff;

  &:hover {
    opacity: 0.8;
  }

  &:active {
    opacity: 0.5;
  }

  img {
    ${props => props.isBack && 'transform: scaleX(-1)'};
  }
`
