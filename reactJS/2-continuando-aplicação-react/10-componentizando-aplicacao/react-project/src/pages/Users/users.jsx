import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'


import axios from 'axios'

import * as S from './styles'

import Title from '@/components/Title/Title'
import Trash from '@/assets/trash.svg'
import Arrow from '@/assets/arrow.svg'

export default function Users() {
  const [users, setUsers] = useState([])
  const navigate = useNavigate()

  useEffect(() => {
    async function loadUsers() {
      const { data: newUser } = await axios.get(
        'https://node-users-dev.herokuapp.com/users'
      )
      setUsers(newUser)
    }
    loadUsers()
  }, [])

  async function deleteUser(userId) {
    await axios.delete(`https://node-users-dev.herokuapp.com/users/${userId}`)
    setUsers(users.filter(user => user.id !== userId))
  }

  function goBackPage() {
    navigate('/')
  }

  return (
    <S.Container>
      <Title>Lista de usuários</Title>
      <S.Table>
        <S.TableHeader>
          <S.TRHeader>
            <S.THHeader>ID</S.THHeader>
            <S.THHeader>Nome</S.THHeader>
            <S.THHeader>Email</S.THHeader>
          </S.TRHeader>
        </S.TableHeader>
        {users.map(user => (
          <S.TableBody key={user.id}>
            <S.TRow>
              <S.TData>{user.id}</S.TData>
              <S.TData>{user.name}</S.TData>
              <S.TData>
                {user.email}
                <S.Image
                  onClick={() => deleteUser(user.id)}
                  alt="seta"
                  src={Trash}
                />
              </S.TData>
            </S.TRow>
          </S.TableBody>
        ))}
      </S.Table>
      <S.Button onClick={goBackPage}>
        <img alt="seta" src={Arrow} /> voltar
      </S.Button>
    </S.Container>
  )
}
