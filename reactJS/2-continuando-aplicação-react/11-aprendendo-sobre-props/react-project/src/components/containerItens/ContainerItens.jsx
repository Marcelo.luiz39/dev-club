//criar components com snippets rfc

import React from 'react'
import { ContainerItens as Container } from './styles'

 function ContainerItens(props) {
  return (
    <Container>{props.children}</Container>
  )
}

export default ContainerItens
