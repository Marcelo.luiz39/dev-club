/**
 * Classes
 
Modelo com objects

const person = {
  name: 'Marcelo Luiz',
  age: 42,
  talk: function () {
    console.log(`Hello, my name is ${this.name}`)
  },
}
*/

class Person {
  name
  age
  talk() {
    console.log(`Hello, my name is ${this.name} and I'm ${this.age} years old`)
  }
}

class Person1 {
  constructor(name, age) {
    console.log(`Hello, I'm ${name}`)

    this.name = name
    this.age = age
    /* this é utilizado para expor a variável para o escopo global da 
       função
    */
  }
  talk() {
    console.log(`Hello, my name is ${this.name} and I'm ${this.age} years old`)
  }
}

const newPersonMarcelo = new Person()
newPersonMarcelo.name = 'Marcelo'
newPersonMarcelo.age = 43
newPersonMarcelo.talk()

const newPersonJulia = new Person1('Julia', 18)
newPersonJulia.talk()

const newPersonPereira = new Person1('Marcelo Pereira', 43)
newPersonPereira.talk()

/**
 * Criando objetos caso seja poucos, usamos spreed
 */

const user = {
  name: 'Marcelo',
  email: 'mlluiz@gmail.com',
  login: () => 'O usuário fez login',
  logout: () => 'O usuário fez logout',
}

const user0 = {
  ...user,
  name: 'Julia Souza',
  email: 'Julia@gmail.com',
}

console.log(user, user0)

class User {
  constructor(name, email) {
    this.name = name
    this.email = email
  }
}

const user1 = new User('Marcelo Luiz', 'mlluiz@gmail.com')
const user2 = new User('Julia Souza', 'juuh@gmail.com')

console.log(user1, user2)
